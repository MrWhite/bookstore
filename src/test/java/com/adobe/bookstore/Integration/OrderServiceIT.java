package com.adobe.bookstore.Integration;


import org.json.JSONException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class OrderServiceIT {

    @BeforeAll
    static void setUp() {
        baseURI = "http://localhost:8080";
        port = 8080;
    }

    @Test
    public void givenUrl_createOrderCheckStatusAndResponse() {
        Map<String, String> book = new HashMap<>();
        book.put("bookId", "22d580fc-d02e-4f70-9980-f9693c18f6e0");
        book.put("quantity", "2");

        ArrayList<Map<String, String>> books = new ArrayList<>();
        books.add(book);

        HashMap<String, ArrayList<Map<String, String>>> request = new HashMap<>();
        request.put("bookList", books);

        given()
                .contentType("application/json")
                .body(request)
                .post("/order")
                .then()
                .statusCode(201)
                .assertThat()
                .body("orderId", equalTo(1));

    }

    @Test
    public void givenUrl_getOrdersCheckSuccessAndOrderCreatedExists() {
        get("/order")
                .then()
                .statusCode(200)
                .assertThat()
                .body("$", hasItem(
                        allOf(
                                hasEntry("orderId", 1)
                        ))
                );
    }

    @Test
    public void givenUrl_getOrdersCheckSuccessAndOrderCreatedExistsInJSON() {
        get("/order?format=json")
                .then()
                .statusCode(200)
                .assertThat()
                .body("$", hasItem(
                        allOf(
                                hasEntry("orderId", 1)
                        ))
                );
    }
    @Test
    public void givenUrl_getOrdersCheckSuccessAndOrderCreatedExistsInCSV() {
        get("/order?format=csv")
                .then()
                .statusCode(200)
                .assertThat()
                .body(containsString("OrderId, Status, Order\n" +"1, Success, [22d580fc-d02e-4f70-9980-f9693c18f6e0(2)]\n")
                );
    }

}