package com.adobe.bookstore.unitTest;


import com.adobe.bookstore.DTOs.ArticleDto;
import com.adobe.bookstore.DTOs.OrderDetailDto;
import com.adobe.bookstore.DTOs.OrderDto;
import com.adobe.bookstore.domain.Article;
import com.adobe.bookstore.repositories.ArticleRepository;
import com.adobe.bookstore.repositories.OrderDetailRepository;
import com.adobe.bookstore.repositories.OrderRepository;
import com.adobe.bookstore.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.NotSupportedException;
import java.util.ArrayList;

@SpringBootTest
public class OrderServiceTest {

    @Autowired
    private OrderService orderService;
    private OrderRepository orderRepository;
    private ArticleRepository articleRepository;
    private OrderDetailRepository orderDetailRepository;


    @Test
    public void validatesEmptyOrderIsInValid()
    {
        //arrange
        ArrayList<OrderDetailDto> orderDetailList= new ArrayList<>();
        //act
        boolean response = orderService.isValidOrder(orderDetailList);
        //assert
        assert(!response) ;
    }

    @Test
    public void validatesCorrectOrderIsValid()
    {
        //arrange
        ArrayList<OrderDetailDto> orderDetailList= new ArrayList<>();
        orderDetailList.add(new OrderDetailDto("22d580fc-d02e-4f70-9980-f9693c18f6e0",1));
        orderDetailList.add(new OrderDetailDto("d02b58ae-8731-451c-9acb-1941adf88501",2));
        //act
        boolean response = orderService.isValidOrder(orderDetailList);
        //assert
        assert(response) ;
    }

    @Test
    public void validatesCorrectOrderWithSingleObjectIsValid()
    {
        //arrange
        ArrayList<OrderDetailDto> orderDetailList= new ArrayList<>();
        orderDetailList.add(new OrderDetailDto("22d580fc-d02e-4f70-9980-f9693c18f6e0",1));

        //act
        boolean response = orderService.isValidOrder(orderDetailList);
        //assert
        assert(response) ;
    }
    @Test
    public void validatesOrderWithInvalidIDIsInvalid()
    {
        //arrange
        ArrayList<OrderDetailDto> orderDetailList= new ArrayList<>();
        orderDetailList.add(new OrderDetailDto("not real ID",1));

        //act
        boolean response = orderService.isValidOrder(orderDetailList);
        //assert
        assert(!response) ;
    }
    @Test
    public void validatesOrderWithExcesiveQuantityIsInvalid()
    {
        //arrange
        ArrayList<OrderDetailDto> orderDetailList= new ArrayList<>();
        orderDetailList.add(new OrderDetailDto("22d580fc-d02e-4f70-9980-f9693c18f6e0",10));

        //act
        boolean response = orderService.isValidOrder(orderDetailList);
        //assert
        assert(!response) ;
    }

    @Test
    public void ConvertsOrderListToCSVString() throws NotSupportedException {
        //arrange
        ArrayList<OrderDto> orderList = new ArrayList<>();
        ArrayList<ArticleDto> articles = new ArrayList<>();
        articles.add(new ArticleDto("articleID", 5));
        articles.add(new ArticleDto("articleID2", 15));
        orderList.add(new OrderDto(1, true, articles));
        //act
        String result= orderService.getCsv(orderList);
        //assert
        assert(result.equals("OrderId, Status, Order\n" +
                "1, Success, [articleID(5) | articleID2(15)]\n"));

    }
}


