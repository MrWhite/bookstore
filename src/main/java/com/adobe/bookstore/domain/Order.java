package com.adobe.bookstore.domain;

import javax.persistence.*;

@Entity
@Table(name="OrderPersisted")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @Column
    boolean isSuccess=false;


    public Order() {
    }

    public Order(boolean isSuccess) {

        this.isSuccess = isSuccess;
    }

    public long getId() {
        return id;
    }



    public boolean isSuccess() {
        return isSuccess;
    }


}