package com.adobe.bookstore.domain;



import com.adobe.bookstore.DTOs.ArticleDto;

import javax.persistence.*;

@Entity
public class OrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    //No relations over hibernate, because I'm directly implementing this relation on my code
    @Column
    public long orderId;

    @Column
    public String articleId;

    @Column
    public int quantity;

    public OrderDetail(long orderId, String articleId, int quantity) {
        this.orderId = orderId;
        this.articleId = articleId;
        this.quantity = quantity;
    }

    public OrderDetail() {

    }

    public ArticleDto toDTO() { return new ArticleDto(this.articleId, this.quantity);}

}