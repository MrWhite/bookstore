package com.adobe.bookstore.DTOs;

public class ArticleDto {
    public String articleId;

    public int quantity;

    public ArticleDto(String articleId, int quantity) {
        this.articleId = articleId;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return this.articleId + ", " + this.quantity;
    }
}

