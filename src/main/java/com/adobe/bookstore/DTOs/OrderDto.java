package com.adobe.bookstore.DTOs;

import java.util.List;

public class OrderDto {

    public long orderId;

    public boolean isSuccess;

    public List<ArticleDto> articlesInOrder;


    public OrderDto(long orderId, boolean isSuccess, List<ArticleDto> articlesInOrder) {
        this.orderId = orderId;
        this.isSuccess = isSuccess;
        this.articlesInOrder = articlesInOrder;
    }


}
