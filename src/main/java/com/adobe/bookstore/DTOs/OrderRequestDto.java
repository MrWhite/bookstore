package com.adobe.bookstore.DTOs;

import java.util.ArrayList;

public class OrderRequestDto {

    public ArrayList<OrderDetailDto> bookList;

    public OrderRequestDto(ArrayList<OrderDetailDto> bookList) {
        this.bookList = bookList;
    }

    public OrderRequestDto() {
    }
}
