package com.adobe.bookstore.DTOs;

public class OrderDetailDto {

    public String bookId;
    public int quantity;

    public OrderDetailDto(String bookId, int quantity) {
        this.bookId = bookId;
        this.quantity = quantity;
    }

    public OrderDetailDto() {
    }
}
