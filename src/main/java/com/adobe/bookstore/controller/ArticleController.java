package com.adobe.bookstore.controller;

import com.adobe.bookstore.domain.Article;
import com.adobe.bookstore.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    public ArticleController(ArticleService articleService) {

        this.articleService = articleService;
    }

    @GetMapping("/")
    public ResponseEntity<Iterable<Article>> list(){
        return ResponseEntity.ok(articleService.list());
    }
}
