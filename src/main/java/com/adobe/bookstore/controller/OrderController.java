package com.adobe.bookstore.controller;

import com.adobe.bookstore.DTOs.ArticleDto;
import com.adobe.bookstore.DTOs.OrderCreatedDto;
import com.adobe.bookstore.DTOs.OrderDto;
import com.adobe.bookstore.DTOs.OrderRequestDto;
import com.adobe.bookstore.domain.Order;
import com.adobe.bookstore.service.InvalidOrderException;
import com.adobe.bookstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.NotSupportedException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    public ResponseEntity<?> addOrder(@RequestBody OrderRequestDto newOrder) {
        try {
            Order resultOrder = orderService.addOrder(newOrder);
            return ResponseEntity.status(HttpStatus.CREATED).body(new OrderCreatedDto(resultOrder.getId()));
        } catch (InvalidOrderException e) {
            return ResponseEntity.badRequest().body("Some Books are not in Stock or are not valid");
        }
    }

    @GetMapping
    public ResponseEntity<?> getAllOrders(HttpServletResponse response, @RequestParam( required = false) String format) throws NotSupportedException {
        ArrayList<OrderDto> allOrders = orderService.getAllOrders();

        if(format==null || format.equals("json") ) {
            response.setContentType("application/json");
            return ResponseEntity.ok(allOrders);
        }

        if(format.equals("csv")) {
            response.setContentType("application/csv");
            response.setHeader("Content-Disposition", "attachment; filename=\"orders.csv\"");

            OutputStream outputStream ;
            try {
                outputStream = response.getOutputStream();
                String outputResult = orderService.getCsv(allOrders);
                outputStream.write(outputResult.getBytes());
                outputStream.flush();
                outputStream.close();
                return ResponseEntity.ok("");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        throw new NotSupportedException("The format " + format + " is not supported");
    }


}
