package com.adobe.bookstore;

import com.adobe.bookstore.service.ArticleService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.adobe.bookstore.domain.Article;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@SpringBootApplication
public class BookstoreApplication {
	private static final Logger LOG = LoggerFactory.getLogger(BookstoreApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(BookstoreApplication.class, args);
	}


	@Bean
	CommandLineRunner runner(ArticleService storeService){
		return args -> {
			//read json and write to db
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Article>> typeReference = new TypeReference<>() {
			};
			InputStream inputStream = TypeReference.class.getResourceAsStream("/json/stock.json");
			try {
				List<Article> articles =  mapper.readValue(inputStream, typeReference);
				storeService.save(articles);
				LOG.info("Articles Saved!!");

			} catch (IOException e){
				LOG.error("Unable to save Articles: " + e.getMessage());

			}
		};
	}

}
