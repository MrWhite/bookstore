package com.adobe.bookstore.repositories;

import com.adobe.bookstore.domain.OrderDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface OrderDetailRepository extends CrudRepository<OrderDetail, Long> {
    ArrayList<OrderDetail> findByOrderId(long order);
}
