package com.adobe.bookstore.repositories;


import com.adobe.bookstore.domain.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ArticleRepository extends CrudRepository<Article, String> {
}
