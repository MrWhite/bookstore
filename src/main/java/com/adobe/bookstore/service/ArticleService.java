package com.adobe.bookstore.service;


import com.adobe.bookstore.domain.Article;
import com.adobe.bookstore.repositories.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public Iterable<Article> list(){
        return articleRepository.findAll();
    }

    public Article save(Article article) {
        return articleRepository.save(article);
    }

    public  Iterable<Article> save(List<Article> articles) {
        return articleRepository.saveAll(articles);
    }
}
