package com.adobe.bookstore.service;

import com.adobe.bookstore.DTOs.ArticleDto;
import com.adobe.bookstore.DTOs.OrderDetailDto;
import com.adobe.bookstore.DTOs.OrderDto;
import com.adobe.bookstore.DTOs.OrderRequestDto;
import com.adobe.bookstore.domain.Article;
import com.adobe.bookstore.domain.Order;
import com.adobe.bookstore.domain.OrderDetail;
import com.adobe.bookstore.repositories.ArticleRepository;
import com.adobe.bookstore.repositories.OrderDetailRepository;
import com.adobe.bookstore.repositories.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.NotSupportedException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private static final Logger LOG = LoggerFactory.getLogger(OrderService.class);

    private final OrderRepository orderRepository;
    private final ArticleRepository articleRepository;
    private final OrderDetailRepository orderDetailRepository;

    public OrderService(OrderRepository orderRepository, ArticleRepository articleRepository, OrderDetailRepository orderDetailRepository) {
        this.orderRepository = orderRepository;
        this.articleRepository = articleRepository;
        this.orderDetailRepository = orderDetailRepository;
    }

    public Order addOrder(OrderRequestDto newOrder) throws InvalidOrderException {

        if(!isValidOrder(newOrder.bookList))
            throw new InvalidOrderException();

        Order createdOrder = orderRepository.save(new Order(true));
        createOrderDetail(newOrder, createdOrder);
        updateArticle(newOrder.bookList);

        return createdOrder;

    }

    protected void createOrderDetail(OrderRequestDto newOrder, Order createdOrder) {
        for(OrderDetailDto detail: newOrder.bookList){
            OrderDetail detailToSave = new OrderDetail(createdOrder.getId(), detail.bookId, detail.quantity);
            orderDetailRepository.save(detailToSave);
        }
    }

    protected void updateArticle(ArrayList<OrderDetailDto> bookList) {
        for(OrderDetailDto detail: bookList) {
            try {
                Article articleToUpdate = articleRepository.findById(detail.bookId).orElseThrow();
                articleToUpdate.setQuantity(articleToUpdate.getQuantity() - detail.quantity);
                articleRepository.save(articleToUpdate);
            } catch (Exception e) {
                LOG.error("Error updating stock of article: " + detail.bookId);
            }
        }
    }

    public boolean isValidOrder(ArrayList<OrderDetailDto> newOrder) {

        if(newOrder.isEmpty()){
            return false;
        }
        for(OrderDetailDto detail: newOrder){
            Optional<Article> currentArticle = articleRepository.findById(detail.bookId);
            if(currentArticle.isPresent()){
                if(currentArticle.get().getQuantity()<detail.quantity){
                    return false;
                }
            }else {
                return false;
            }
        }
        return true;

    }

    public ArrayList<OrderDto> getAllOrders() {
        ArrayList<OrderDto> allOrders2Return = new ArrayList<>();
        try {
            Iterable<Order> allOrders = orderRepository.findAll();
            for(Order order: allOrders){
                List<ArticleDto> articles =  orderDetailRepository
                        .findByOrderId(order.getId())
                        .stream()
                        .map(OrderDetail::toDTO)
                        .collect(Collectors.toList());

                OrderDto orderDto= new OrderDto(order.getId(), order.isSuccess(),articles);
                allOrders2Return.add(orderDto);

            }
        } catch (Exception e) {
            return null;
        }
        return allOrders2Return;
    }

    protected static final String CSV_SEPARATOR = ", ";
    public String getCsv(ArrayList<OrderDto> orderList) throws NotSupportedException {
        StringBuilder builder = new StringBuilder();
        builder.append("OrderId, Status, Order");
        builder.append(System.getProperty("line.separator"));

        try {
            for (OrderDto order : orderList) {
                builder.append(order.orderId <= 0 ? "" : order.orderId);
                builder.append(CSV_SEPARATOR);
                builder.append(order.isSuccess ? "Success" : "Fail");
                builder.append(CSV_SEPARATOR);

                builder.append("[");
                int index = 1;
                for (ArticleDto article : order.articlesInOrder) {
                    builder.append(article.articleId);
                    builder
                            .append("(")
                            .append(article.quantity)
                            .append(")");
                    if (index < order.articlesInOrder.size()) {
                        builder.append(" | ");
                        index++;
                    }
                }
                builder.append("]");
                builder.append(System.getProperty("line.separator"));
            }

            return builder.toString();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }

        throw new NotSupportedException();
    }


}
